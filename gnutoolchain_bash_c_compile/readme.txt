Test inputs for the script assignment.sh

#   Create a static library
1)  ./compile.sh -staticlib ./libhanoi.a src/hanoi*

#   Create a shared library
2)  ./compile.sh -sharedlib libfunc.so ./src/func*

#   Create a program and link shared and static libraries
3)  ./compile.sh ./tstprog -lhanoi src/*.c -lfunc

#   Create a program without libraries
4)  ./compile.sh newprog src/*
