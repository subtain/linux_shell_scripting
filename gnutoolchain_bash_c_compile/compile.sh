#!/bin/bash

#Uncomment to see complete compiler output
#COMPILE_VERBOSE=-v
#LINK_VERBOSE=-v

err_report() {
    echo "Error on line $1"
}

trap 'err_report $LINENO' ERR

prj_path=$(dirname $0)

#File to store paths of static and shared libray when compiling
libcache=$prj_path/libpath.temp
[[ ! -f $libcache ]] && touch $libcache

function write_file {
    lib_str=$(cat $libcache)
    lib_str=${lib_str/$1/}
    lib_str+=" $1"
    #Remove leading whitespaces from lib_str
    lib_str=$(echo -e "${lib_str}" | sed -e 's/^[[:space:]]*//')
    printf "%s" "$lib_str" > $libcache
}

#Project directories
lib=$2
app=$1

#Set compiler and flags
: ${CC:="/usr/bin/gcc"}
: ${CCFLAGS:="-Wall -c -fmessage-length=0 -I/usr/include"}
: ${LDFLAGS:="-L/usr/lib -Wl,-rpath=/usr/lib"}

#Library flags
LIBFLAGS=""
[[ $1 = "-sharedlib" ]] && LIBFLAGS="-fpic"
if ( [[ $1 = "-staticlib" ]] || [[ $1 = "-sharedlib" ]] ); then
    lib_file=$(basename $2)
    LIBEXT="${lib_file##*.}"
fi

OBJS=""
LIBS=""

#Compile source code into object files
for arg in $@
do
    #Filter header file, object file and library file in source dir
    if ( [[ $arg != -* ]] && ! [[ $arg =~ \.h$ ]] \
        && ! [[ $arg =~ \.o$ ]] && ! [[ $arg =~ \.${LIBEXT}$ ]] ); then

        src_file=$(basename $arg)
        obj_file=${src_file/.*/.o}

        if [[ $obj_file =~ \.o$ ]]; then
            src_path=$(dirname $arg)
            obj_file=$src_path/$obj_file
            OBJS+=" $obj_file"

            rm -vf $obj_file
            
            ${CC} $COMPILE_VERBOSE $LIBFLAGS ${CCFLAGS} -I$src_path -o $obj_file -x c $arg
            [[ $? -ne 0 ]] && exit 1
        fi
    fi
    
    #Test positional argument starts with -l
    [[ $arg =~ ^-l ]] && LIBS+=" $arg"
done

#Create static library
if [[ $1 = "-staticlib" ]]; then
    lib_path=$(realpath $2)
    lib_path=$(dirname $lib_path)

    echo Creating static library $lib
    ar rcs $lib $OBJS
    [[ $? -ne 0 ]] && exit 2
    
    #Write directory path of static library to file for linking 
    write_file "-L${lib_path} -Wl,-rpath=${lib_path}"
    #Remove shared library of same name
    rm -vf ${lib/.a/.so}
    exit 0
fi

#Create shared library
if [[ $1 = "-sharedlib" ]]; then
    lib_path=$(realpath $2)
    lib_path=$(dirname $lib_path)

    echo Creating shared library $lib
    ${CC} -shared -Wl,-soname,$(basename $lib) -o $lib $OBJS
    [[ $? -ne 0 ]] && exit 3

    #Write directory path of shared library to file for linking 
    write_file "-L${lib_path} -Wl,-rpath=${lib_path}"
    #Remove static library of same name
    rm -vf ${lib/.so/.a}
    exit 0
fi

#Create executable
echo Creating executable $app
#Add project libraries path information from the file 
LDFLAGS+=" $(cat $libcache)"
${CC} $LINK_VERBOSE -o $app $OBJS ${LDFLAGS} $LIBS
[[ $? -ne 0 ]] && exit 4

exit 0
