
#ifndef FUNC_H_
#define FUNC_H_

#include <stdio.h>
#include <stdint.h>

void my_program(uint32_t *);
void my_print(uint32_t);

#endif
