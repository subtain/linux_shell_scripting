/*      This program provides the solution to the famous        */
/*      Hanoi problem written in ANSI C.                        */

#include <stdio.h>
#include "hanoi.h"
#include "func.h"

/* This functions takes a tower of n disks and moves from peg   */
/* 'source' to peg 'destination'.  Peg 'temp' may be used       */
/* temporarily.                                                 */

void __attribute__((constructor)) program_init(void) {
	printf("init constructor\n");
}

void __attribute__((destructor)) program_fini(void) {
	printf("fini destructor\n");
}

int main(void)
{
	int n;
	int var;
	var = 0;
	
	my_program(&var);
	my_print(var);

	printf("Please enter the number of disks: ");
	scanf("%d",&n);
	printf("\n");

	Hanoi('A','B','C',n);
	
	return 0;
}

